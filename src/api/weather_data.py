from sanic import Blueprint, json
from injector import inject, singleton

from services import WeatherService

@singleton
class WeatherDataController:

    @inject
    def __init__(self, weather_service: WeatherService):
        self.weather_service = weather_service

    async def get(self, request):
        locations = request.args.getlist('locations')
        if not locations:
            return json({"error": "Please provide location"}, status=400)
        fields = request.args.getlist('fields')
        if not fields:
            return json({"error": "Please provide data field name"}, status=400)

        requested_data = await self.weather_service.get_weather_by_locations_and_fields(locations, fields)
        return json(requested_data)

    
def create_weather_controller(weather_controller: WeatherDataController, app):
    weather_controller_bp = Blueprint('weather')

    @weather_controller_bp.get('/weather_data')
    async def decorated_get_weather_data(request):
        return await weather_controller.get(request)

    app.blueprint(weather_controller_bp)
