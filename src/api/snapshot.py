from sanic import Blueprint, json
from injector import inject, singleton

from services import SnapshotService

@singleton
class SnapshotController:

    @inject
    def __init__(self, snapshot_service: SnapshotService):
        self.snapshot_service = snapshot_service

    async def post(self, request, trigger_type: str):
        try:
            await self.snapshot_service.take_snapshot(trigger_type)
        except Exception as e:
            return json({"Error": "Can't create snapshot"}, status=400)
        return json({"Message": "Snapshot created successfully"}, status=201)
    
def create_snapshot_controller(snapshot_controller: SnapshotController, app):
    snapshot_controller_bp = Blueprint('snapshot')

    @snapshot_controller_bp.post('/snapshot/<trigger_type>')
    def decorated_post_snapshot(request, trigger_type):
        return snapshot_controller.post(request, trigger_type)

    app.blueprint(snapshot_controller_bp)
