from sanic import Blueprint, json
from injector import inject, singleton
from services.snapshot_db_service import SnapshotDBService

@singleton
class HealthCheckController:

    @inject
    def __init__(self, snapshot_db_service: SnapshotDBService):
        self._snapshot_db_service = snapshot_db_service

    async def status(self, request):
        errors = []
        mongo_status = await self._snapshot_db_service.get_status()
        if (mongo_status['code'] != 200):
            errors.append(mongo_status['message'])
        return json({ "message": "ok" })

    
def create_healthcheck_controller(healthcheck_controller: HealthCheckController, app):
    healthcheck_controller_bp = Blueprint('healthcheck')

    @healthcheck_controller_bp.route('/healthcheck')
    def decorated_health_check(request):
        return healthcheck_controller.status(request)

    app.blueprint(healthcheck_controller_bp)
