from injector import singleton
from sanic.log import logger

@singleton
class LoggerService:
    def error(self, message):
        logger.error(message)

    def info(self, message):
        logger.info(message)