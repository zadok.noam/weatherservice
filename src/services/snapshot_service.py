import asyncio
from datetime import datetime, timezone
from typing import Dict
from injector import singleton, inject
from services import SnapshotDBService
from services import WeatherService
from services.logger_service import LoggerService
from os import environ

@singleton
class SnapshotService:
    @inject
    def __init__(self, weather_service: WeatherService, snapshot_db_service: SnapshotDBService, logger_service: LoggerService):
        self._weather_service = weather_service
        self._snapshot_db_service = snapshot_db_service
        self.logger_service = logger_service
        self._snapshots_collection = environ.get("SNAPSHOTS_COLLECTION")
        self.thresholds = {
            "temp": int(environ.get("TEMP_THRESHOLD")),
                "humidity": int(environ.get("HUMIDITY_THRESHOLD")),
                "visibility": int(environ.get("VISIBILITY_THRESHOLD")),
                "wind_speed": int(environ.get("WIND_SPEED_THRESHOLD"))
        }

    async def take_snapshot(self, trigger_type: str) -> None:
        timestamp = datetime.now(tz=timezone.utc)
        try:
            weather_data = await self._weather_service.get_weather_data()
        except Exception as e:
            self.logger_service.error(f"error with getting weather data: {e}")
        snapshot_data = []
        for location, records in weather_data.items():
            location_record = {}
            location_record['trigger_type'] = trigger_type
            location_record['timestamp'] = timestamp
            location_record['location'] = location
            location_record['processed'] = False
            for key, val in records.items():
                location_record[key] = val
            snapshot_data.append(location_record)
        try:
            await self._snapshot_db_service.create_data(self._snapshots_collection, snapshot_data)
        except Exception as e:
            self.logger_service.error(f"Error saving data to db: {e}")

    async def process_snapshot(self) -> None:
        snapshot_records = await self._snapshot_db_service.get_data(self._snapshots_collection, {'processed': False})
        updated_records = []
        for snapshot in snapshot_records:
            updated_fields = {}
            updated_fields['processed'] = True
            try:
                updated_fields['risk_level'] = self.__check_risk_level(snapshot)
            except Exception:
                await self._snapshot_db_service.delete_single_record(self._snapshots_collection, snapshot)
                raise Exception({"Error": f"Could not process snapshot with trigger type {snapshot['trigger_type']}"})
            updated_records.append(self._snapshot_db_service.update_record(self._snapshots_collection, snapshot, updated_fields))
        await asyncio.gather(*updated_records)
        self.logger_service.info(f"Processed {len(updated_records)} snapshots")


    def __check_risk_level(self, snapshot_record: Dict[str, str]) -> str:
        triger_type = snapshot_record['trigger_type']
        try:
            data = snapshot_record[triger_type]
        except Exception as e:
            self.logger_service.error(f"Wrong trigger_type \"triger_type\"")
            raise e

        if triger_type == 'temp':
            print(data, self.thresholds['temp'])
            if data > self.thresholds['temp']:
                return 'High'
        if triger_type == 'humidity':
            if data > self.thresholds['humidity']:
                return 'High'
        if triger_type == 'visibility':
            if data < self.thresholds['visibility']:
                return 'High'
        if triger_type == 'wind_speed':
            if data > self.thresholds['wind_speed']:
                return 'High'
        
        return 'Low'