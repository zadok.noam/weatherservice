import asyncio
import json
from typing import Dict, List
from injector import inject
from aiohttp import ClientSession
from services.caching_service import CachingService
from services.locations_service import LocationsService
from services.logger_service import LoggerService
from services.time_series_db_service import TimeSeriesDBService
from os import environ


class WeatherService:
    @inject
    def __init__(
        self,
        locations_service: LocationsService,
        caching_service: CachingService,
        time_series_db_service: TimeSeriesDBService,
        logger_service: LoggerService,
    ):
        self.locations_service = locations_service
        self.caching_service = caching_service
        self.time_series_db_service = time_series_db_service
        self.logger_service = logger_service
        self.weather_api_key = environ.get("API_KEY")
        self.weather_api_url = environ.get("API_URL")

    async def sample_weather_data(self) -> None:
        weather_data = await self.get_weather_data()
        self.__write_weather_to_cache(weather_data)
        await self.__write_weather_to_db(weather_data)
        self.logger_service.info("sampling done")

    async def get_weather_by_locations_and_fields(
        self, locations: List[str], fields: List[str]
    ) -> Dict[str, Dict[str, str]]:

        weather_data = {}
        for location in locations:
            # check if data in cache
            cache_data = self.caching_service.get(location)
            if cache_data:
                dict_fields = {k: v for k, v in cache_data.items() if k in fields}
                weather_data[location] = dict_fields
            else:
                # check if data in db
                db_data = await self.time_series_db_service.get_measurement_last_data(
                    location, fields
                )
                if db_data:
                    weather_data[location] = db_data
                else:
                    # no data found
                    weather_data[location] = {"Error": "No data found for location"}
        return weather_data

    async def get_weather_data(self) -> Dict[str, Dict[str, str]]:
        locations = await self.locations_service.get_locations()
        weather_results = []
        for coords in locations:
            weather = self.__get_weather_from_api(coords["lat"], coords["lng"])
            weather_results.append(weather)
        results = await asyncio.gather(*weather_results)
        locations_with_weather = {}
        for location in results:
            json_data = json.loads(location)
            locations_with_weather[json_data["name"]] = json_data
        return self.__process_data(locations_with_weather)

    async def __get_weather_from_api(self, lat: float, lng: float):
        url = f"{self.weather_api_url}lat={lat}&lon={lng}&appid={self.weather_api_key}&units=metric"
        async with ClientSession() as session:
            async with session.get(url) as response:
                if response.status == 200:
                    html = await response.text()
                    return html
        return None

    def __process_data(
        self, data: Dict[str, Dict[str, str]]
    ) -> Dict[str, Dict[str, str]]:
        processed_data = {}
        for key, value in data.items():
            temp = value["main"]["temp"]
            humidity = value["main"]["humidity"]
            visibility = value["visibility"]
            wind_speed = value["wind"]["speed"]
            processed_data[key] = {
                "temp": temp,
                "humidity": humidity,
                "visibility": visibility,
                "wind_speed": wind_speed,
            }
        return processed_data

    def __write_weather_to_cache(self, data: Dict[str, Dict[str, str]]) -> None:
        try:
            self.caching_service.set_multiple_values(data)
            self.logger_service.info("Finished caching data")
        except Exception as e:
            self.logger_service.error(f"exception during data caching: {e}")

    async def __write_weather_to_db(self, data: Dict[str, Dict[str, str]]) -> None:
        try:
            await self.time_series_db_service.write_data(data)
            self.logger_service.info("Finished writing data to db")
        except Exception as e:
            self.logger_service.error(f"exception during writing to db: {e}")
