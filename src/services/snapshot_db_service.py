from typing import Dict, List
from injector import inject, singleton
from motor.motor_asyncio import AsyncIOMotorClient
from services.logger_service import LoggerService
from os import environ

@singleton
class SnapshotDBService:
    @inject
    def __init__(self, logger_service: LoggerService):
        self.logger_service = logger_service
        self._connection_str = environ.get("MONGO_DB_CONNECTION_STRING")
        self._mongo_db_client = None
        self._db = None

    @property
    def db(self):
        if self._db is None:
            self._mongo_db_client = AsyncIOMotorClient(self._connection_str)
            self._db = self._mongo_db_client[environ.get("DB_NAME")]
        return  self._db

    @property
    def mongo_db_client(self):
        if self._mongo_db_client is None:
            self._mongo_db_client = AsyncIOMotorClient(self._connection_str)
        return  self._mongo_db_client

    async def get_status(self):
        try:
            status = await self.mongo_db_client.server_info()
            if status['ok'] == 1.0:
                return {"code": 200, "message": "ok"}
        except Exception as e:
            pass
        return {"code": 404, "message": "mongo DB error"}

    async def create_data(self, collection: str, data: List[Dict[str, str]]) -> List[Dict[str, str]]:
        try:
            result = await self.db[collection].insert_many(data)
        except Exception as e:
            self.logger_service.error(f"error inserting document: {e}")
            raise Exception("Error writing snapshot to db")
        return result.inserted_ids

    async def get_data(self, collection: str, filter: Dict[str, str]) -> List[Dict[str, str]]:
        results = await self.db[collection].find(filter).to_list(None)
        return results

    async def update_record(self, collection: str, old_record: Dict[str, str], fields: Dict[str, str]):
        id = old_record['_id']
        try:
            await self.db[collection].find_one_and_update(filter={'_id': id}, update={'$set':fields})
        except Exception as e:
            self.logger_service.error(f"Error updating record: {e}")

    async def delete_single_record(self, collection: str, record: Dict[str, str]) -> None:
        try:
            await self.db[collection].delete_one(record)
        except Exception as e:
            self.logger_service.error(f"Could not delete record: {e}")
