import json
from typing import Dict, List
import redis
from injector import inject, singleton
from services.logger_service import LoggerService
from os import environ

@singleton
class CachingService:
    @inject
    def __init__(self, logger_service: LoggerService):
        self.logger_service = logger_service
        try:
            self.redis = redis.Redis(
                host=environ.get("REDIS_HOST"),
                port=environ.get("REDIS_PORT"),
                db=0
            )
        except Exception as e:
            self.logger_service.error(f"Error connecting to redis: {e}")
            raise Exception("Error connecting to caching service")
        self.timeout = int(environ.get("REDIS_KEY_TTL"))

    def set(self, key, val) -> None:
        self.redis.set(key, val, ex=self.timeout)
    
    def get(self, key):
        val = self.redis.get(key)
        try:
            json_val = json.loads(val)
            return json_val
        except:
            return val

    def set_multiple_values(self, data: Dict[str, Dict[str, str]]) -> None:
        pipe = self.redis.pipeline()
        for key, val in data.items():
            pipe.set(key, json.dumps(val), ex=self.timeout)
        pipe.execute()
        self.logger_service.info("done writing to redis")
