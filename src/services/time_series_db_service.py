from typing import Dict, List
from influxdb_client import Point
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
from injector import inject, singleton
from services.logger_service import LoggerService
from os import environ

@singleton
class TimeSeriesDBService:
    @inject
    def __init__(self, logger_service: LoggerService):
        self._org=environ.get("DB_ORGANIZATION")
        self._bucket=environ.get("DB_BUCKET")
        self.api_token=environ.get("INFLUXDB_API_TOKEN")
        self.db_url=environ.get("DB_URL")
        self._influxdb_client = None
        self.logger_service = logger_service

    def __cretae_connection(self) -> None:
        self._influxdb_client = InfluxDBClientAsync(url=self.db_url,
        token=self.api_token,
        org=self._org)
        self.logger_service.info("Influx connection created")

    async def __close_connection(self) -> None:
        await self._influxdb_client.close()
        self.logger_service.info("Influx connection closed")


    async def write_data(self, data: Dict[str, Dict[str, str]]) -> None:
        self.__cretae_connection()
        write_api = self._influxdb_client.write_api()
        data_points = []
        for key, value in data.items():
            point = Point(key)
            for field, field_val in value.items():
                point.field(field, field_val)
            data_points.append(point)

        result = await write_api.write(bucket=self._bucket, record=data_points)
        if not result:
            self.logger_service.error("Error writing to Influx db")
            raise
        self.logger_service.info("writing to Influx successful")
        await self.__close_connection()
        return result

    async def get_measurement_last_data(self, measurement: str, fields: List[str]):
        query = f'from(bucket:"{self._bucket}") '\
            f'|> range(start:-{environ.get("SAMPLING_INTERVAL_TIME_SECONDS")}s)'\
            f'|> filter(fn: (r) => '\
            f'r._measurement == "{measurement}") '
        query += f'|> filter(fn: (r) => '
        for field in fields:
            query += f'r._field == "{field}" or '
        query = query[:-4]
        query += f') |> sort(columns: ["_time"], desc: true) '
        query += f'|> limit(n:1)'

        results = await self.__preform_query(query)
        processed_results = None
        if results:
            processed_results = {}
            for table in results:
                for record in table.records:
                    processed_results[record["_field"]] = record["_value"]
        return processed_results

    async def __preform_query(self, query):
        self.__cretae_connection()
        query_api = self._influxdb_client.query_api()

        try:
            results = await query_api.query(query)
        except Exception as e:
            self.logger_service.error(e)
            results = None
        
        await self.__close_connection()
        return results
