from typing import Dict, List
from injector import inject, singleton
from services.logger_service import LoggerService
from os import environ

@singleton
class LocationsService:
    @inject
    def __init__(self, logger_service: LoggerService):
        self.logger_service = logger_service
        self.locations_file = environ.get("LOCATIONS_FILE")
        self.locations = None

    async def __load_locations(self) -> None:
        self.locations = []
        with open(self.locations_file) as f:
            for line in f:
                lat, lng = line.strip().split(", ")
                self.locations.append({"lat": lat, "lng": lng})

    async def get_locations(self) -> List[Dict[str, str]]:
        if not self.locations:
            try:
                await self.__load_locations()
            except Exception as e:
                self.logger_service.error(f"Error loading locations file: {e}")
                return []
        return self.locations
