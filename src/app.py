from injector import Injector
from sanic import Sanic, text
from api import WeatherDataController, create_weather_controller
from api import HealthCheckController, create_healthcheck_controller
from api import SnapshotController, create_snapshot_controller
from services.snapshot_service import SnapshotService
from services.weather_service import WeatherService
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from dotenv import load_dotenv
from os import environ

load_dotenv()
app = Sanic(name="my_app")

@app.listener('before_server_start')
async def initialize_scheduler(app, loop):
    container = Injector()
    weather_service = container.get(WeatherService)
    snapshot_service = container.get(SnapshotService)
    scheduler = AsyncIOScheduler({'event_loop': loop})
    scheduler.add_job(weather_service.sample_weather_data, 'interval', seconds=int(environ.get("SAMPLING_INTERVAL_TIME_SECONDS")))
    scheduler.add_job(snapshot_service.process_snapshot, 'interval', seconds=int(environ.get("SNAPSHOT_INTERVAL_TIME_SECONDS")))

    scheduler.start()

# async def main(container, scheduler: AsyncIOScheduler):
#     sampling_service = container.get(SamplingService)
#     await 

if __name__ == "__main__":
    container = Injector()

    weather_data_controller = container.get(WeatherDataController)
    create_weather_controller(weather_data_controller, app)

    snapshot_controller = container.get(HealthCheckController)
    create_healthcheck_controller(snapshot_controller, app)

    snapshot_controller = container.get(SnapshotController)
    create_snapshot_controller(snapshot_controller, app)

    app.run(host="0.0.0.0", port=environ.get("SERVICE_PORT"))
