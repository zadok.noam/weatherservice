FROM python:3.7.4-slim AS compile-image
RUN apt-get update

COPY ./requirements.txt ./
RUN set -x && \
    python -m pip install --upgrade pip &&\
    pip install --user -r requirements.txt

COPY ./src ./src
COPY ./locations.txt ./locations.txt

ENV SAMPLING_INTERVAL_TIME_SECONDS=5
ENV SNAPSHOT_INTERVAL_TIME_SECONDS=5
ENV SERVICE_PORT=8080
ENV LOCATIONS_FILE='./locations.txt'
ENV REDIS_HOST='localhost'
ENV REDIS_PORT=6379
ENV REDIS_KEY_TTL=120
ENV MONGO_DB_CONNECTION_STRING='mongodb://127.0.0.1:27017/?directConnection=true&serverSelectionTimeoutMS=2000&appName=mongosh+1.4.2'
ENV DB_NAME='weather_db'
ENV SNAPSHOTS_COLLECTION='snapshots'
ENV TEMP_THRESHOLD=35
ENV HUMIDITY_THRESHOLD=70
ENV VISIBILITY_THRESHOLD=50
ENV WIND_SPEED_THRESHOLD=20
ENV DB_ORGANIZATION='orca-ai'
ENV DB_BUCKET='weather-ex'
ENV INFLUXDB_API_TOKEN='kGKo1beqlCtSebMjZmnpXKexAirX8sFqB14EyQbqLpRZT6240Ald9MYJLq87zrDZUAIUyBFgSc3DKFUrLg1Csg=='
ENV DB_URL='http://localhost:8086'
ENV API_KEY='4f729a485cf3f775dc9937fb38f437a7'
ENV API_URL='https://api.openweathermap.org/data/2.5/weather?'

CMD ["python", "src/app.py"]